<div class="container-fluid">
    <div class="alert alert-primary text-center" role="alert"> <i class="fas fa-edit"></i>
        Edit Data Barang
    </div>

    <?php foreach ($barang as $br) : ?>
        <form action="<?= base_url() . 'admin/data_barang/update' ?>" method="POST">
            <div class="form-group">
                <label>NAMA BARANG</label>
                <input type="hidden" name="id_brg" value="<?= $br->id_brg ?>">
                <input type="text" name="nama_brg" class="form-control" value="<?= $br->nama_brg ?>">
            </div>
            <div class="form-group">
                <label>KETERANGAN</label>
                <textarea type="text" name="keterangan" rows="3" id="tambah_barang" class="form-control"><?= $br->keterangan ?></textarea>
            </div>
            <div class="form-group">
                <label>KATEGORI</label>
                <input type="text" name="kategori" class="form-control" value="<?= $br->kategori ?>">
            </div>
            <div class="form-group">
                <label>HARGA</label>
                <input type="text" name="harga" class="form-control" value="<?= $br->harga ?>">
            </div>
            <div class="form-group">
                <label>STOK</label>
                <input type="text" name="stok" class="form-control" value="<?= $br->stok ?>">
            </div>
            <button type="submit" class="btn btn-sm btn-primary mt-3 mb-3">Simpan</button>
        </form>
    <?php endforeach; ?>
</div>