<?php

class Data_barang extends CI_Controller
{
    public function index()
    {

        $data['judul'] = "Halaman Data Barang";
        $data['barang'] = $this->model_barang->tampil_data()->result();
        $this->load->view('templates_admin/header', $data);
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/data_barang');
        $this->load->view('templates_admin/footer');
    }

    public function tambah_aksi()
    {
        $nama_brg   = $this->input->post('nama_brg');
        $keterangan = $this->input->post('keterangan');
        $kategori   = $this->input->post('kategori');
        $harga      = $this->input->post('harga');
        $stok       = $this->input->post('stok');
        $point      = $this->input->post('point');
        $gambar     = $_FILES['gambar']['name'];
        if ($gambar = '') {
        } else {
            $config['upload_path'] = './upload';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('gambar')) {
                echo "Gambar Gagal Diupload";
            } else {
                $gambar = $this->upload->data('file_name');
            }
        }
        $data = [
            "nama_brg"   => $nama_brg,
            "keterangan" => $keterangan,
            "kategori"   => $kategori,
            "harga"     => $harga,
            "stok"      => $stok,
            "point"      => $point,
            "gambar"      => $gambar
        ];

        $this->model_barang->tambah_barang($data, 'tb_barang');
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Barang Berhasil Ditambahkan!.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>');
        redirect('admin/data_barang');
    }

    public function edit($id)
    {
        $where = ["id_brg" => $id];
        $data['judul'] = "Edit Data Barang";
        $data['barang'] = $this->model_barang->edit_barang($where, 'tb_barang')->result();
        $this->load->view('templates_admin/header', $data);
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/edit_barang', $data);
        $this->load->view('templates_admin/footer');
    }

    public function detail_brg($id_brg)
    {
        $data['barang'] = $this->model_barang->detail_brg($id_brg);
        $data['judul'] = "Halaman Admin Detail Barang";
        $this->load->view('templates_admin/header', $data);
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/detail_barang', $data);
        $this->load->view('templates/footer');
    }

    public function update()
    {
        $id             = $this->input->post('id_brg');
        $nama_brg          = $this->input->post('nama_brg');
        $keterangan     = $this->input->post('keterangan');
        $kategori       = $this->input->post('kategori');
        $harga          = $this->input->post('harga');
        $stok           = $this->input->post('stok');
        $point           = $this->input->post('point');

        $data =
            [
                "nama_brg" => $nama_brg,
                "keterangan" => $keterangan,
                "kategori" => $kategori,
                "harga" => $harga,
                "stok" => $stok,
                "point" => $point
            ];

        $where = [
            "id_brg" => $id
        ];

        $this->model_barang->update_data($where, $data, 'tb_barang');
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Barang Berhasil Diupdate!.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>');
        redirect('admin/data_barang');
    }

    public function hapus($id)
    {
        $where = ["id_brg" => $id];
        $this->model_barang->hapus_data($where, 'tb_barang');
        $this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
            Data Barang Berhasil Dihapus!.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>');
        redirect('admin/data_barang');
    }
}
